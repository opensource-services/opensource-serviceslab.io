#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'stefwalter'
SITENAME = 'Open Source Services'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Utc'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_PAGES_ON_MENU = False

# Blogroll
LINKS = (
        ('Playbooks', '/pages/playbooks.html'),
        ('Contributor Stories', '/pages/user-stories.html'),
        ('Operate First', 'https://operate-first.cloud'),
        ('Open Source', 'https://opensource.org/')
        )

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

STATIC_PATHS = ['images', 'files']

THEME = 'themes/pelican-svbhack'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
