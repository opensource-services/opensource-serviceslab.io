Title: Open Source Services
Save_as: index.html

Most Software as a Service that tries to follow the “Open Source” development model, does so by throwing the code over the wall. This misses the fundamental part of Open Source: allowing contributors to make a change freely and see their own changes easily before including them in the mainline.

Without true Open Source principles, we are missing a way to have a community or ecosystem or sticky center of gravity develop around a Software as a Service.

Certain “Open Source” SaaS projects do include logic on how to deploy/operate the SaaS, but contributors are still not able to run their contributions. This is because a real SaaS includes data, dependencies, deployment artefacts, credentials, API access, monitoring, testing, most of which are not in the source code itself.

For true Open Source, we must enable people to run their contributions to the SaaS “in production” with real data/APIs/privileges. And do so before those contributions are included in the mainline SaaS behaviour, experienced by everyone.

<img alt="Challenge Accepted" src="../images/challenge-accepted.png" width="200px" style="float: right;">

# Open Source Service Challenge

A user of an Open Source Service can:

 * Discover which component to contribute to.

 * Make a nonsensical change: eg: add a printf style log statement or change spelling

 * Experience that change when using the existing service, or when it acts on their data

 * Iterate on that change until it’s useful, and propose it for merging

Communities in an Open Source Service can:

 * Share changes, stabilize new behavior, before everyone experiences those changes

# Why, How and Who?

We'd like to work together to pull off this challenge. Here's some material:

 * [Why is Open Source code insufficient for Services?](http://stef.thewalter.net/open-source-services.html)
 * [How to Open Source cloud operations](https://www.operate-first.cloud/how-to-open-source-cloud-operations.md)
 * [How can this work in practice?](https://blog.tomecek.net/post/open-service-as-a-service-in-practice/)

There's [Playbooks](pages/playbooks.html) for how it might be done and [User Stories](pages/user-stories.html) for how contributors might experience Open Source Services.
